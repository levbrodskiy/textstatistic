package ru.blm.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import ru.blm.filereader.FileReader;
import ru.blm.popupwindows.LogBookWindow;
import ru.blm.textstatistic.TextStatistic;
import ru.blm.popupwindows.ErrorWindow;
import ru.blm.popupwindows.StatisticWindow;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Controller {

    private String text;

    @FXML
    private TextArea inputText;

    @FXML
    private Button getStatisticButton;

    @FXML
    private TextField inputPath;

    @FXML
    private RadioButton fieldRadioButton;

    @FXML
    private RadioButton fileRadioButton;

    @FXML
    void logBookButtonClick(ActionEvent event) {
        try {
            new LogBookWindow().openWindow();
        } catch (Exception e) {
            new ErrorWindow().openWindow("Ошибка Базы Данных!");
        }
    }

    @FXML
    void getStatisticClick(ActionEvent event) {
        if(fieldRadioButton.isSelected()){
            text = inputText.getText();
            openStatisticWindow(formStatistic());
        }
        if (fileRadioButton.isSelected()){
            try {
                text = new FileReader(Paths.get(inputPath.getText())).read();
                openStatisticWindow(formStatistic());
            } catch (IOException e) {
                new ErrorWindow().openWindow("Неверные данные");
            } catch (Exception e){

            }

        }

    }

    private Map formStatistic(){
        TextStatistic statistic = new TextStatistic(text);
        long numberAllSymbols = statistic.countAllSymbols();
        long numberSymbolsWithoutGaps = statistic.countSymbolsWithoutGaps();
        long numberAllWords = statistic.countAllWords();
        long numberAllQuestionMarks = statistic.countAllQuestionMarks();
        long numberAllExclamationMarks = statistic.countAllExclamationMarks();
        long numberAllCommas = statistic.countAllCommas();
        long numberAllDots = statistic.countAllDots();
        long numberAllGaps = statistic.countAllGaps();

        Map<String, Long> statisticData = new HashMap();
        statisticData.put("numberAllSymbols", numberAllSymbols);
        statisticData.put("numberSymbolsWithoutGaps", numberSymbolsWithoutGaps);
        statisticData.put("numberAllWords", numberAllWords);
        statisticData.put("numberAllQuestionMarks", numberAllQuestionMarks);
        statisticData.put("numberAllExclamationMarks", numberAllExclamationMarks);
        statisticData.put("numberAllCommas", numberAllCommas);
        statisticData.put("numberAllDots", numberAllDots);
        statisticData.put("numberAllGaps", numberAllGaps);

        return statisticData;
    }

    private void openStatisticWindow( Map<String, Long> statisticData) {
        new StatisticWindow().openWindow(statisticData);
    }
}
