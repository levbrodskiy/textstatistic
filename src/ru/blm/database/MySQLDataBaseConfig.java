package ru.blm.database;

public interface MySQLDataBaseConfig {
    String URL = "jdbc:mysql://localhost:3306/logbook?serverTimezone=Europe/Moscow&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}
