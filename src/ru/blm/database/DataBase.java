package ru.blm.database;


import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataBase implements MySQLDataBaseConfig {
    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (Exception e) {

        }
    }

    private static Connection getConnection() throws Exception{
        Connection connection = null;

            Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);


        return connection;
    }

    public void insert(long countAllSymbols, long countAllSymbolsWithoutGaps, long countAllGaps, long countAllWords, long countAllDots, long countAllCommas, long countAllExclaminationMarks, long countAllQuestionMarks) throws SQLException {
        Date date = new Date();
        String currentDate = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
        String query = "insert into logbook(date, count_all_symbols, count_symbols_without_gaps," +
                "count_all_gaps, count_all_words, count_all_dots, count_all_commas," +
                "count_all_exclamation_marks, count_all_question_marks) values(" + currentDate + ","
                + countAllSymbols + ","
                + countAllSymbolsWithoutGaps + ","
                + countAllGaps + ","
                + countAllWords + ","
                + countAllDots + ","
                + countAllCommas + ","
                + countAllExclaminationMarks + ","
                + countAllQuestionMarks
                + ");";
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    public List<String> select() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "select * from logbook;";
        ResultSet resultSet = connection.createStatement().executeQuery(query);

        while(resultSet.next()){
            String data = "ID: " + resultSet.getInt("id") + " / " +
                    "Дата: " + resultSet.getString("date") + " / " +
                    "Количество слов: " + resultSet.getString("count_all_symbols")+ " / " +
                    "Количество слов: " + resultSet.getString("count_symbols_without_gaps")+ " / " +
                    "Количество слов: " + resultSet.getString("count_all_gaps")+ " / " +
                    "Количество слов: " + resultSet.getString("count_all_words")+ " / " +
                    "Количество слов: " + resultSet.getString("count_all_dots")+ " / " +
                    "Количество слов: " + resultSet.getString("count_all_commas")+ " / " +
                    "Количество слов: " + resultSet.getString("count_all_exclamation_marks")+ " / " +
                    "Количество слов: " + resultSet.getString("count_all_question_marks")+ " ; ";
            result.add(data);
        }
        return result;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }
}
