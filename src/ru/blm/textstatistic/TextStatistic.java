package ru.blm.textstatistic;

public class TextStatistic {
    private String text;

    public TextStatistic(String text) {
        this.text = text;
    }

    /**
     * Метод возвращает количество символов в тексте
     *
     * @return количество символов в тексте
     */
    public long countAllSymbols() {
        return text.length();
    }

    /**
     * Метод возвращает количество символов в тексте( без пробелов )
     *
     * @return количество символов в тексте( без пробелов )
     */
    public long countSymbolsWithoutGaps() {
        String textWithoutGaps = text.replaceAll("\\s", "");
        return textWithoutGaps.length();
    }

    /**
     * Возвращает количество пробелов в тексте
     * @return количество пробелов в тексте
     */
    public long countAllGaps() {
        return countAllSymbols() - countSymbolsWithoutGaps();
    }

    /**
     * Возвращает количество точек в тексте
     * @return количество точек в тексте
     */
    public long countAllDots() {
        String textWithoutDots = text.replaceAll("\\.", "");
        return countAllSymbols() - textWithoutDots.length();
    }

    /**
     * Возвращает количество запятых в тексте
     * @return количество запятых в тексте
     */
    public long countAllCommas() {
        String textWithoutCommas = text.replaceAll(",", "");
        return countAllSymbols() - textWithoutCommas.length();
    }

    /**
     * Возвращает количество вопросительных знаков в тексте
     * @return количество вопросительных знаков в тексте
     */
    public long countAllQuestionMarks() {
        String countAllQuestionMarks = text.replaceAll("\\?", "");
        return countAllSymbols() - countAllQuestionMarks.length();
    }

    /**
     * Возвращает количество восклицательных знаков в тексте
     * @return количество восклицательных знаков в тексте
     */
    public long countAllExclamationMarks() {
        String countAllExclamationMarks = text.replaceAll("!", "");
        return countAllSymbols() - countAllExclamationMarks.length();
    }
    /**
     * Метод возвращает количество слов в тексте
     *
     * @return количество слов в тексте
     */
    public long countAllWords() {
        int numberOfWords = 0;
        String textReplace = text.replaceAll("[\"\\\\\\[\\]\\/\\*\\+\\.\\,\\(\\)\\&\\?\\%\\$\\#\\@\\!\\;\\№]+"," ");
        String[] splitLine = textReplace.split("\\s");

        for (int i = 0; i < splitLine.length; i++) {
            if(splitLine[i].isEmpty()||splitLine[i] == ""||splitLine[i].matches("[-]+")){
                continue;
            }
            numberOfWords++;
        }
        return numberOfWords;
    }
}
