package ru.blm.popupwindows;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import ru.blm.database.DataBase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class LogBookWindow {
    public void openWindow() throws SQLException {
        Stage stage = new Stage();


        Collection<String> logsFromDB = new ArrayList<>();
        DataBase dataBase = new DataBase();
        List<String> result = dataBase.select();

        ObservableList<String> log = FXCollections.observableArrayList(result);
        ListView<String> logListView = new ListView<String>(log);
        logListView.setPrefWidth(1100);
        logListView.setPrefHeight(200);
        FlowPane root = new FlowPane(logListView);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Журнал");
        stage.show();
    }
}
