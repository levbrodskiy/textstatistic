package ru.blm.popupwindows;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import ru.blm.database.DataBase;

import java.sql.SQLException;
import java.util.Map;

public class StatisticWindow {
    public void openWindow(Map<String, Long> map) {
        Stage stage = new Stage();

        Label numberAllSymbols = new Label("Кол-во символов: " + map.get("numberAllSymbols").toString());
        numberAllSymbols.setLayoutX(10);
        numberAllSymbols.setLayoutY(10);

        Label numberSymbolsWithoutGaps = new Label("Кол-во символов без пробелов: " + map.get("numberSymbolsWithoutGaps").toString());
        numberSymbolsWithoutGaps.setLayoutX(10);
        numberSymbolsWithoutGaps.setLayoutY(40);

        Label numberAllWords = new Label("Кол-во слов: " + map.get("numberAllWords").toString());
        numberAllWords.setLayoutX(10);
        numberAllWords.setLayoutY(70);

        Label numberAllQuestionMarks = new Label("Кол-во вопросительных знаков: " + map.get("numberAllQuestionMarks").toString());
        numberAllQuestionMarks.setLayoutX(10);
        numberAllQuestionMarks.setLayoutY(100);

        Label numberAllExclamationMarks = new Label("Кол-во восклицательных знаков: " + map.get("numberAllExclamationMarks").toString());
        numberAllExclamationMarks.setLayoutX(10);
        numberAllExclamationMarks.setLayoutY(130);

        Label numberAllCommas = new Label("Кол-во точек: " + map.get("numberAllCommas").toString());
        numberAllCommas.setLayoutX(10);
        numberAllCommas.setLayoutY(160);

        Label numberAllDots = new Label("Кол-во запятых: " + map.get("numberAllDots").toString());
        numberAllDots.setLayoutX(10);
        numberAllDots.setLayoutY(190);

        Label numberAllGaps = new Label("Кол-во пробелов: " + map.get("numberAllGaps").toString());
        numberAllGaps.setLayoutX(10);
        numberAllGaps.setLayoutY(220);

        Group group = new Group(numberAllSymbols, numberSymbolsWithoutGaps, numberAllWords,
                numberAllQuestionMarks, numberAllExclamationMarks, numberAllCommas, numberAllDots, numberAllGaps);
        Scene scene = new Scene(group);

        stage.setScene(scene);
        stage.setWidth(300);
        stage.setHeight(300);

        stage.setTitle("Статистика");
        stage.show();

        DataBase dataBase = new DataBase();
        try {
            dataBase.insert(map.get("numberAllSymbols"),
                    map.get("numberSymbolsWithoutGaps"),
                    map.get("numberAllGaps"),
                    map.get("numberAllWords"),
                    map.get("numberAllDots"),
                    map.get("numberAllCommas"),
                    map.get("numberAllQuestionMarks"),
                    map.get("numberAllCommas")
                    );
        } catch (SQLException e) {

        }catch (Exception e){

        }
    }
}
